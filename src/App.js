import './App.css';
import Menu from "./Components/Menu/Menu";
import Order from "./Components/Order/Order";

function App() {
    return (
        <div className="App">
            <div style={{display: 'flex', maxWidth: '700px', margin: '0 auto'}}>
                <Menu/>
            </div>
        </div>
    );
}

export default App;
