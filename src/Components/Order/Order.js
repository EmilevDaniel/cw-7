import React from 'react';
import $ from 'jquery'

const Order = props => {
    $('.OrderText').show();
    const myMenu = props.menu.map(e => {
        if (e.count > 0) {
            $('.OrderText').hide();
            return (<div key={e.id} className={e.name}>{e.name} x{e.count} {e.price}KGZ <button
                onClick={() => props.removeItem(e.name)}>Delete</button></div>)
        }
    });

    const totalPrice = props.menu.map(e => {
        return e.count * e.price
    }).reduce((acc, cur) => acc + cur, 0);


    return (
        <div className='orderHolder' style={{width: '50%', border: '2px solid grey'}}>
            <p className='OrderText'>Order is empty!</p>
            <p className='OrderText'>Please add some items!</p>
            {myMenu}
            <div>Total price: {totalPrice}</div>
        </div>
    );
};

export default Order;