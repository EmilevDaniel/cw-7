import React, {useState} from 'react';
import {nanoid} from "nanoid";
import Order from "../Order/Order";

const Menu = () => {
    const [menu, setMenu] = useState([
        {name: 'Hamburger', count: 0, price: 80, id: nanoid()},
        {name: 'Coffee', count: 0, price: 70, id: nanoid()},
        {name: 'Cheeseburger', count: 0, price: 90, id: nanoid()},
        {name: 'Tea', count: 0, price: 50, id: nanoid()},
        {name: 'Fries', count: 0, price: 45, id: nanoid()},
        {name: 'Cola', count: 0, price: 40, id: nanoid()},
    ]);

    const addItem = name => {
        setMenu(menu.map(p => {
            if (name === p.name) {
                return {...p, count: p.count + 1}
            }
            return p;
        }))
    };

    const removeItem = name => {
        setMenu(menu.map(p => {
            if (name === p.name && p.count > 0) {
                return {...p, count: p.count - 1}
            }
            return p;
        }))
    };

    const myMenu = menu.map(e => (
        <div key={e.id} onClick={() => addItem(e.name)}
             style={{display: 'flex', width: '50%', border: '2px solid grey'}}>
            <p>Image</p>
            <div>
                <h3 style={{fontSize: '12px'}}>{e.name}</h3>
                <p>Price: {e.price} KGS</p>
            </div>
        </div>
    ));

    return (
        <>
            <Order menu={menu} removeItem={removeItem}/>
            <div style={{display: 'flex', flexWrap: 'wrap', width: '50%'}}>
                {myMenu}
            </div>

        </>
    );
};

export default Menu;